package com.rvir.theboaringapp.fragmenti.nasveti;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.util.InteresAPIsHandler;
import com.rvir.theboaringapp.util.KuharskiRecept;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

public class ReceptNasvetFragment extends Fragment {

    private FragmentActivity fragmentActivity;

    public static ReceptNasvetFragment newInstance() {
        return new ReceptNasvetFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new PridobiKuharskiRecept(fragmentActivity).execute();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recept_nasvet_fragment, container, false);
        return view;
    }

    private static class PridobiKuharskiRecept extends AsyncTask<Void, Void, KuharskiRecept> {

        private WeakReference<FragmentActivity> activityReference;
        private ProgressDialog pDialog;

        PridobiKuharskiRecept(FragmentActivity context) {
            activityReference = new WeakReference<>(context);
            pDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Prosimo počakajte...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected KuharskiRecept doInBackground(Void... arg0) {
            return new InteresAPIsHandler().getRecipeFromApi();
        }

        @Override
        protected void onPostExecute(KuharskiRecept kuharskiRecept) {

            FragmentActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            ImageView image = activity.findViewById(R.id.recipe_image);
            Picasso.get().load(kuharskiRecept.getLinkDoSlike()).into(image);

            TextView title = activity.findViewById(R.id.title);
            title.setText(kuharskiRecept.getNaziv());

            TextView time = activity.findViewById(R.id.time);
            time.setText(kuharskiRecept.getCasPriprave());

            TextView calories = activity.findViewById(R.id.calories);
            calories.setText(kuharskiRecept.getCalories());

            TextView weight = activity.findViewById(R.id.weight);
            weight.setText(kuharskiRecept.getTeza());

            ListView sestavine = activity.findViewById(R.id.sestavine);
            ArrayAdapter adapter = new ArrayAdapter<>(activity.getApplicationContext(), android.R.layout.simple_list_item_1, kuharskiRecept.getSestavine());
            sestavine.setAdapter(adapter);

            if (kuharskiRecept.getSestavine().size() == 0) {
                Toast.makeText(activityReference.get(), "Napaka pri pridobivanju podatkov", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentActivity = getActivity();
        } catch (ClassCastException castException) {
            Log.i("Fragment", "Napaka: " + castException.getMessage());
        }
    }

    @Override
    public void onDetach() {
        fragmentActivity = null;
        super.onDetach();
    }
}
