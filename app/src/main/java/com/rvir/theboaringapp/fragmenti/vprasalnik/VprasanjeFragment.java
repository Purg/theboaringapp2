package com.rvir.theboaringapp.fragmenti.vprasalnik;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.IFragmentListener;

public class VprasanjeFragment extends Fragment {

    private IFragmentListener rbcListener;
    private String vprasanje;
    private int pozicija;

    public static VprasanjeFragment newInstance(String vprasanje, int pozicija) {
        Log.d("vprasanjeFragment", "kličem newInstance("+vprasanje+", "+pozicija+")");
        VprasanjeFragment vprasanjeFragment = new VprasanjeFragment();
        Bundle args = new Bundle();
        args.putString("vprasanje", vprasanje);
        args.putInt("pozicija", pozicija);
        vprasanjeFragment.setArguments(args);
        return vprasanjeFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("vprasanjeFragment", "prožim onCreate()");
        vprasanje = getArguments().getString("vprasanje");
        pozicija = getArguments().getInt("pozicija");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Log.d("vprasanjeFragment", "prožim onCreateView()");
        View view = inflater.inflate(R.layout.vprasanje_fragment, container, false);

        if(pozicija == 3) {
            Button button = view.findViewById(R.id.naprej_button);
            button.setVisibility(View.VISIBLE);

            button.setOnClickListener(view1 -> rbcListener.onNaprejButtonClicked());
        }

        TextView tv = view.findViewById(R.id.vprasanjeText);

        String vprasanjeString = "";
        switch (pozicija) {
            case 1:
                vprasanjeString = getString(R.string.vprasanje1);
                break;

            case 2:
                vprasanjeString = getString(R.string.vprasanje2);
                break;

            case 3:
                vprasanjeString = getString(R.string.vprasanje3);
                break;

            default:
                break;
        }

        tv.setText(vprasanjeString);

        RadioGroup rg = view.findViewById(R.id.radio_group_vprasanje);

        rg.setOnCheckedChangeListener((group, checkedId) -> {
            Log.d("vprasanjeFragment", "prožim setOnCheckedChangeListener");
            switch(checkedId)
            {
                case R.id.radio_da:
                    rbcListener.setFragPozicijaChecked(1, pozicija);
                    break;
                case R.id.radio_ne:
                    rbcListener.setFragPozicijaChecked(0, pozicija);
                    break;
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            rbcListener = (IFragmentListener) context;
        } catch (ClassCastException castException) {
            Log.i("frag", "napaka: " + castException.getMessage());
        }
    }

    @Override
    public void onDetach() {
        rbcListener = null;
        super.onDetach();
    }
}