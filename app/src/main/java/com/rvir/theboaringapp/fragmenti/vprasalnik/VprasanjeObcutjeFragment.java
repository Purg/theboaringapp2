package com.rvir.theboaringapp.fragmenti.vprasalnik;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.IFragmentListener;

public class VprasanjeObcutjeFragment extends Fragment {

    private IFragmentListener rbcListener;

    public static VprasanjeObcutjeFragment newInstance() {
        VprasanjeObcutjeFragment vprasanjeObcutjeFragment = new VprasanjeObcutjeFragment();
        Bundle args = new Bundle();
        vprasanjeObcutjeFragment.setArguments(args);
        return vprasanjeObcutjeFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vprasanje_obcutje_fragment, container, false);
        RadioGroup rg = view.findViewById(R.id.radio_group_obcutje);

        rg.setOnCheckedChangeListener((group, checkedId) -> {
            switch(checkedId)
            {
                case R.id.radio_obc1:
                    rbcListener.setFrag1Checked(1);
                    break;
                case R.id.radio_obc2:
                    rbcListener.setFrag1Checked(2);
                    break;

            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            rbcListener = (IFragmentListener) context;
        } catch (ClassCastException castException) {
            Log.i("frag", "napaka: " + castException.getMessage());
        }
    }

    @Override
    public void onDetach() {
        rbcListener = null;
        super.onDetach();
    }
}
