package com.rvir.theboaringapp.fragmenti.nasveti;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.rvir.theboaringapp.MapsActivity;
import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.db.entitete.Nasvet;
import com.rvir.theboaringapp.places.GetNearbyPlaces;
import com.tooltip.Tooltip;

/**
 * v bistvu skopirano iz MapsActivity, ampak context pridobi v metodi onAttach() - na dnu razreda
 */
public class GoogleMapsNasvetFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = MapsActivity.class.getSimpleName();

    private String query;
    private FragmentActivity fragmentActivity;

    private GoogleMap mMap;
    private Button tooltip_gumb;

    private CameraPosition mCameraPosition;

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;

    // A default location (Sydney, Australia) and default zoom to use when location permission is
    // not granted.
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    public static GoogleMapsNasvetFragment newInstance(Nasvet nasvet) {
        Log.d(TAG, "kličem newInstance("+nasvet.getOpis()+", "+nasvet.getNazivNasveta().getVrsta()+")");
        GoogleMapsNasvetFragment googleMapsNasvetFragment = new GoogleMapsNasvetFragment();
        Bundle args = new Bundle();
        args.putString("query", nasvet.getNazivNasveta().getVrsta());
        googleMapsNasvetFragment.setArguments(args);
        return googleMapsNasvetFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        query = getArguments().getString("query");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_maps, container, false);

        tooltip_gumb = view.findViewById(R.id.button_tooltip);
        tooltip_gumb.setOnClickListener(view1 -> {
            Tooltip tooltip = new Tooltip.Builder(view1)
                    .setText("Vse lokacije morda niso\nustrezne. Prosimo Vas, da o\ntem presodite sami.")
                    .setTextColor(Color.WHITE)
                    .setBackgroundColor(Color.BLACK)
                    .setGravity(Gravity.BOTTOM)
                    .setCornerRadius(5f)
                    .setDismissOnClick(true)
                    .show();
        });

        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(fragmentActivity);

        // Build the map.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return view;
    }

    /**
     * Manipulates the map when it's available.
     * This callback is triggered when the map is ready to be used.
     */
    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        // Prompt the user for permission.
        getLocationPermission();

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();

        Toast.makeText(fragmentActivity, "Prikazujemo bližnja mesta..", Toast.LENGTH_SHORT).show();
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(fragmentActivity, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mMap.clear();

                            mLastKnownLocation = task.getResult();

                            LatLng latLng = new LatLng(mLastKnownLocation.getLatitude(),
                                    mLastKnownLocation.getLongitude());

                            mMap.addMarker(new MarkerOptions()
                                    .title(getString(R.string.map_current_location))
                                    .position(latLng)
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    latLng, DEFAULT_ZOOM));

                            GetNearbyPlaces getNearbyPlaces = new GetNearbyPlaces(fragmentActivity);
                            Object transferData[] = new Object[2];

                            String url = getUrl(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude(), query);
                            transferData[0] = mMap;
                            transferData[1] = url;

                            getNearbyPlaces.execute(transferData);

                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }


    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(fragmentActivity.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(fragmentActivity,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private String getUrl(double latitide, double longitude, String nearbyPlace) {
        StringBuilder googleURL = new StringBuilder(
                "https://maps.googleapis.com/maps/api/place/nearbysearch/json?radius=10000&key=AIzaSyDUQjb3wUHCLM9aCOHSafxVkRNa3oEWvh0&sensor=true");
        googleURL.append("&location=").append(latitide).append(",").append(longitude);
        googleURL.append("&name=").append(nearbyPlace);

        Log.d("GoogleMapsActivity", "url = " + googleURL.toString());

        return googleURL.toString();
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                Log.d("google maps", "array velikost: " + grantResults.length);
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
                break;
            }
        }
        updateLocationUI();
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                Log.d("google maps", "ni uspelo dobit lokacije");
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentActivity = getActivity();
        } catch (ClassCastException castException) {
            Log.i("frag", "napaka: " + castException.getMessage());
        }
    }

    @Override
    public void onDetach() {
        fragmentActivity = null;
        super.onDetach();
    }
}
