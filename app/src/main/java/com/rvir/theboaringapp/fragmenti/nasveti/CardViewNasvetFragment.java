package com.rvir.theboaringapp.fragmenti.nasveti;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.db.entitete.Nasvet;
import com.rvir.theboaringapp.interesadapters.GlasbaRecyclerViewAdapter;
import com.rvir.theboaringapp.interesadapters.MovieRecyclerViewAdapter;
import com.rvir.theboaringapp.util.InteresAPIsHandler;
import com.rvir.theboaringapp.util.PopularniFilmiSerije;
import com.rvir.theboaringapp.util.TopTrack;

import java.lang.ref.WeakReference;
import java.util.List;

public class CardViewNasvetFragment extends Fragment {

    private static final String URL_FILMI = "https://api.themoviedb.org/3/movie/popular?api_key=2911b5389a356b51c23c907f6c29d609&language=en-US&page=1";
    private static final String URL_SERIJE = "https://api.themoviedb.org/3/tv/popular?api_key=2911b5389a356b51c23c907f6c29d609&language=en-US&page=1";

    private FragmentActivity fragmentActivity;
    private String cardviewName;

    public static CardViewNasvetFragment newInstance(Nasvet nasvet) {
        CardViewNasvetFragment cardViewNasvetFragment = new CardViewNasvetFragment();
        Log.d("cardViewFrag", "naziv: " + nasvet.getNazivNasveta().getVrsta());
        Bundle args = new Bundle();
        args.putString("current_cardview", nasvet.getNazivNasveta().getVrsta());
        cardViewNasvetFragment.setArguments(args);
        return cardViewNasvetFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cardviewName = getArguments().getString("current_cardview");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.cardview_nasvet_fragment, container, false);

        switch (cardviewName) {
            case "poslušanje glasbe":
                new PridobiGlasbo(fragmentActivity).execute();
                break;
            case "film":
                new PridobiFilmeSerije(fragmentActivity, URL_FILMI).execute();
                break;
            case "serija":
                new PridobiFilmeSerije(fragmentActivity, URL_SERIJE).execute();
                break;
            default:
                break;
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentActivity = getActivity();
        } catch (ClassCastException castException) {
            Log.i("fragment", "Napaka: " + castException.getMessage());
        }
    }

    @Override
    public void onDetach() {
        fragmentActivity = null;
        super.onDetach();
    }

    private static class PridobiFilmeSerije extends AsyncTask<Void, Void, List<PopularniFilmiSerije>> {

        private WeakReference<FragmentActivity> activityReference;
        private ProgressDialog pDialog;
        private String url;

        PridobiFilmeSerije(FragmentActivity context, String url) {
            activityReference = new WeakReference<>(context);
            pDialog = new ProgressDialog(context);
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Prosimo počakajte...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected List<PopularniFilmiSerije> doInBackground(Void... arg0) {
            return new InteresAPIsHandler().getPopularMovies(url);
        }

        @Override
        protected void onPostExecute(List<PopularniFilmiSerije> popularniFilmiSerijeList) {

            FragmentActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            RecyclerView recyclerView = activity.findViewById(R.id.recyclerview_card_list);
            MovieRecyclerViewAdapter movieRecyclerViewAdapter = new MovieRecyclerViewAdapter(activity.getApplicationContext(), popularniFilmiSerijeList);
            recyclerView.setLayoutManager(new GridLayoutManager(activity.getApplicationContext(), 2));
            recyclerView.setAdapter(movieRecyclerViewAdapter);

            if (popularniFilmiSerijeList.size() == 0) {
                Toast.makeText(activityReference.get(), "Napaka pri pridobivanju podatkov", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static class PridobiGlasbo extends AsyncTask<Void, Void, List<TopTrack>> {

        private WeakReference<FragmentActivity> activityReference;
        private ProgressDialog pDialog;

        PridobiGlasbo(FragmentActivity context) {
            activityReference = new WeakReference<>(context);
            pDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Prosimo počakajte...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected List<TopTrack> doInBackground(Void... arg0) {
            return new InteresAPIsHandler().getTopTracks();
        }

        @Override
        protected void onPostExecute(List<TopTrack> topTrackList) {

            FragmentActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            RecyclerView recyclerView = activity.findViewById(R.id.recyclerview_card_list);
            GlasbaRecyclerViewAdapter glasbaRecyclerViewAdapter = new GlasbaRecyclerViewAdapter(activity.getApplicationContext(), topTrackList);
            recyclerView.setLayoutManager(new GridLayoutManager(activity.getApplicationContext(), 2));
            recyclerView.setAdapter(glasbaRecyclerViewAdapter);

            if (topTrackList.size() == 0) {
                Toast.makeText(activityReference.get(), "Napaka pri pridobivanju podatkov", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

