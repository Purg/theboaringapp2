package com.rvir.theboaringapp.db.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.rvir.theboaringapp.db.MyDatabase;
import com.rvir.theboaringapp.db.dao.KategorijaNasvetDao;
import com.rvir.theboaringapp.db.entitete.KategorijaNasvet;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class KategorijaNasvetViewModel extends AndroidViewModel {

    private KategorijaNasvetDao kategorijaDao;
    private ExecutorService executorService;

    public KategorijaNasvetViewModel(@NonNull Application application) {
        super(application);
        kategorijaDao = MyDatabase.getInstance(application).kategorijaNasvetDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public KategorijaNasvet dobiInteres(int id) {
        return kategorijaDao.findOne(id);
    }

    public ArrayList<KategorijaNasvet> dobiInterese() {
        return (ArrayList<KategorijaNasvet>) kategorijaDao.findAll();
    }

    public void posodobiInterese(final List<KategorijaNasvet> kategorijaNasvetiList) {
        executorService.execute(() -> kategorijaDao.updateAll(kategorijaNasvetiList.toArray(new KategorijaNasvet[0])));
    }

    public void shraniKategorija(final KategorijaNasvet interes) {
        executorService.execute(() -> kategorijaDao.save(interes));
    }

    public void zbrisiKategorija(final KategorijaNasvet interes) {
        executorService.execute(() -> kategorijaDao.delete(interes));
    }
}
