package com.rvir.theboaringapp.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.rvir.theboaringapp.db.entitete.Vprasanje;

import java.util.List;

@Dao
public interface VprasanjeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long save(Vprasanje vprasanje);

    @Insert
    void insertAll(Vprasanje... vprasanja);

    @Update
    void update(Vprasanje vprasanje);

    @Delete
    void delete(Vprasanje vprasanje);

    @Query("SELECT * FROM Vprasanje")
    LiveData<List<Vprasanje>> findAll();

    @Query("SELECT vprasanje FROM Vprasanje")
    List<String> findAllList();

    @Query("SELECT * FROM Vprasanje WHERE id = :id")
    Vprasanje findOne(int id);
}
