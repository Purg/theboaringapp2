package com.rvir.theboaringapp.db.entitete;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;

@Entity(primaryKeys = { "vprasalnikId", "nasvetId" },
        indices = {@Index(value = {"nasvetId"})},
        foreignKeys = {
                @ForeignKey(entity = Vprasalnik.class,
                        parentColumns = "id",
                        childColumns = "vprasalnikId"),
                @ForeignKey(entity = Nasvet.class,
                        parentColumns = "id",
                        childColumns = "nasvetId")
        })

public class VprasalnikNasvet {
    private int vprasalnikId;
    private int nasvetId;
    private Boolean doberNasvet;

    public VprasalnikNasvet(int vprasalnikId, int nasvetId, Boolean doberNasvet) {
        this.vprasalnikId = vprasalnikId;
        this.nasvetId = nasvetId;
        this.doberNasvet = doberNasvet;
    }

    public int getVprasalnikId() {
        return vprasalnikId;
    }

    public void setVprasalnikId(int vprasalnikId) {
        this.vprasalnikId = vprasalnikId;
    }

    public int getNasvetId() {
        return nasvetId;
    }

    public void setNasvetId(int nasvetId) {
        this.nasvetId = nasvetId;
    }

    public Boolean isDoberNasvet() {
        return doberNasvet;
    }

    public void setDoberNasvet(Boolean doberNasvet) {
        this.doberNasvet = doberNasvet;
    }

    public static VprasalnikNasvet[] populateData() {
        return new VprasalnikNasvet[] {

//                jeza, noter, sam, ima čas
                new VprasalnikNasvet(1, 2, null),
                new VprasalnikNasvet(1, 3, null),
                new VprasalnikNasvet(1, 4, null),
                new VprasalnikNasvet(1, 5, null),
                new VprasalnikNasvet(1, 12, null),
                new VprasalnikNasvet(1, 13, null),

//                jeza, zunaj, sam, nima časa
                new VprasalnikNasvet(2, 5, null),
                new VprasalnikNasvet(2, 10, null),

//                jeza, noter, s prijatelji, ima čas
                new VprasalnikNasvet(3, 2, null),
                new VprasalnikNasvet(3, 3, null),
                new VprasalnikNasvet(3, 8, null),
                new VprasalnikNasvet(3, 13, null),

//                jeza, zunaj, s prijatelji, ima čas
                new VprasalnikNasvet(4, 6, null),
                new VprasalnikNasvet(4, 7, null),
                new VprasalnikNasvet(4, 14, null),
                new VprasalnikNasvet(4, 15, null),

//       #---------------------------------------------------------------------------------#
//                žalost, noter, sam, ima čas
                new VprasalnikNasvet(5, 2, null),
                new VprasalnikNasvet(5, 3, null),
                new VprasalnikNasvet(5, 4, null),
                new VprasalnikNasvet(5, 5, null),
                new VprasalnikNasvet(5, 11, null),

//                žalost, zunaj, sam, nima časa
                new VprasalnikNasvet(6, 5, null),
                new VprasalnikNasvet(6, 10, null),

//                žalost, noter, s prijatelji, ima čas
                new VprasalnikNasvet(7, 2, null),
                new VprasalnikNasvet(7, 3, null),
                new VprasalnikNasvet(7, 8, null),

//                žalost, zunaj, s prijatelji, ima čas
                new VprasalnikNasvet(8, 6, null),
                new VprasalnikNasvet(8, 7, null),
                new VprasalnikNasvet(8, 14, null),
                new VprasalnikNasvet(8, 15, null),
                new VprasalnikNasvet(8, 16, null),

//       #---------------------------------------------------------------------------------#
//                zaskrbljenost, noter, sam, ima čas
                new VprasalnikNasvet(9, 2, null),
                new VprasalnikNasvet(9, 3, null),
                new VprasalnikNasvet(9, 4, null),
                new VprasalnikNasvet(9, 5, null),
                new VprasalnikNasvet(9, 11, null),
                new VprasalnikNasvet(9, 12, null),
                new VprasalnikNasvet(9, 13, null),

//                zaskrbljenost, zunaj, sam, nima časa
                new VprasalnikNasvet(10, 5, null),
                new VprasalnikNasvet(10, 10, null),

//                zaskrbljenost, noter, s prijatelji, ima čas
                new VprasalnikNasvet(11, 2, null),
                new VprasalnikNasvet(11, 3, null),
                new VprasalnikNasvet(11, 13, null),

//                zaskrbljenost, zunaj, s prijatelji, ima čas
                new VprasalnikNasvet(12, 6, null),
                new VprasalnikNasvet(12, 7, null),
                new VprasalnikNasvet(12, 14, null),
                new VprasalnikNasvet(12, 15, null),
                new VprasalnikNasvet(12, 16, null),
        };
    }
}
