package com.rvir.theboaringapp.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.rvir.theboaringapp.db.entitete.Vprasalnik;

import java.util.List;

@Dao
public interface VprasalnikDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long save(Vprasalnik vprasalnik);

    @Insert
    void insertAll(Vprasalnik... vprasalniki);

    @Update
    void update(Vprasalnik vprasalnik);

    @Delete
    void delete(Vprasalnik vprasalnik);

    @Query("SELECT * FROM Vprasalnik")
    LiveData<List<Vprasalnik>> findAll();

    @Query("SELECT * FROM Vprasalnik WHERE id = :id")
    Vprasalnik findOne(int id);
}
