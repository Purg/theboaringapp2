package com.rvir.theboaringapp.db.entitete.poizvedbe;

import android.arch.persistence.room.Entity;

/**
 * Entiteta, ki se uporablja izključno za pridobivanje vprašalnika, ki ustreza izbranim odgovorom
 */
@Entity
public class UjemajocVprasalnik {
    private int ujemajociOdgovori;
    private int vprasalnikId;

    public UjemajocVprasalnik(int ujemajociOdgovori, int vprasalnikId){
        this.ujemajociOdgovori = ujemajociOdgovori;
        this.vprasalnikId = vprasalnikId;

    }

    public int getUjemajociOdgovori() {
        return ujemajociOdgovori;
    }

    public void setUjemajociOdgovori(int ujemajociOdgovori) {
        this.ujemajociOdgovori = ujemajociOdgovori;
    }

    public int getVprasalnikId() {
        return vprasalnikId;
    }

    public void setVprasalnikId(int vprasalnikId) {
        this.vprasalnikId = vprasalnikId;
    }
}
