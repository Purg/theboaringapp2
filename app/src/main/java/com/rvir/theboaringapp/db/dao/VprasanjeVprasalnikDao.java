package com.rvir.theboaringapp.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.rvir.theboaringapp.db.entitete.poizvedbe.UjemajocVprasalnik;
import com.rvir.theboaringapp.db.entitete.VprasanjeVprasalnik;

import java.util.List;

@Dao
public interface VprasanjeVprasalnikDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long save(VprasanjeVprasalnik vprasanjeVprasalnik);

    @Insert
    void insertAll(VprasanjeVprasalnik... vprasanjeVprasalniki);

    @Update
    void update(VprasanjeVprasalnik vprasanjeVprasalnik);

    @Delete
    void delete(VprasanjeVprasalnik vprasanjeVprasalnik);

    @Query("SELECT * FROM VprasanjeVprasalnik")
    LiveData<List<VprasanjeVprasalnik>> findAll();

    @Query("SELECT * FROM VprasanjeVprasalnik WHERE vprasanjeId = :vprasanjeId and vprasalnikId = :vprasalnikId")
    VprasanjeVprasalnik findOne(int vprasanjeId, int vprasalnikId);

    /**
     * Glede na izbrano občutje in odgovore na vprašanja, poišče vprašalnik, ki se ujema
     * @param obcutjeId id obcutja
     * @param odgovor1 odgovor na 1. vprašanje (true/false)
     * @param odgovor2 odgovor na 2. vprašanje (true/false)
     * @param odgovor3 odgovor na 3. vprašanje (true/false)
     * @return objekt, ki vsebuje vprašalnik z največ ujemajočimi odgovori
     */
    @Query("SELECT COUNT(tabela.vprasanjeId) as ujemajociOdgovori, tabela.vprasalnikId as vprasalnikId\n" +
            "FROM (SELECT DISTINCT VprasanjeVprasalnik.*\n" +
            "\t  FROM Vprasanje, VprasanjeVprasalnik, Vprasalnik\n" +
            "\t  WHERE Vprasalnik.obcutjeId = :obcutjeId AND Vprasalnik.id = VprasanjeVprasalnik.vprasalnikId and \n" +
            "\t\t((VprasanjeVprasalnik.vprasanjeId = 1 AND VprasanjeVprasalnik.odgovor = :odgovor1) OR \n" +
            "\t\t (VprasanjeVprasalnik.vprasanjeId = 2 AND VprasanjeVprasalnik.odgovor = :odgovor2) OR\n" +
            "\t\t (VprasanjeVprasalnik.vprasanjeId = 3 AND VprasanjeVprasalnik.odgovor = :odgovor3))) as tabela\n" +
            "GROUP BY tabela.vprasalnikId\n" +
            "ORDER BY COUNT(tabela.vprasanjeId) DESC LIMIT 1")
    UjemajocVprasalnik findVprasalnik(int obcutjeId, boolean odgovor1, boolean odgovor2,
                                      boolean odgovor3);
}
