package com.rvir.theboaringapp.db.entitete;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Vprasanje {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String vprasanje;

    public Vprasanje(int id, String vprasanje) {
        this.id = id;
        this.vprasanje = vprasanje;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVprasanje() {
        return vprasanje;
    }

    public void setVprasanje(String vprasanje) {
        this.vprasanje = vprasanje;
    }

    public static Vprasanje[] populateData() {
        return new Vprasanje[] {
                new Vprasanje(0, "Želite svoj čas preživeti na prostem?"),
                new Vprasanje(0, "Si želite druženja s prijtelji?"),
                new Vprasanje(0, "Imate na voljo veliko prostega časa?")
        };
    }
}
