package com.rvir.theboaringapp.db.entitete;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;

@Entity(primaryKeys = { "vprasanjeId", "vprasalnikId" },
        indices = {@Index(value = {"vprasalnikId"})},
        foreignKeys = {
                @ForeignKey(entity = Vprasanje.class,
                        parentColumns = "id",
                        childColumns = "vprasanjeId"),
                @ForeignKey(entity = Vprasalnik.class,
                        parentColumns = "id",
                        childColumns = "vprasalnikId")
        })

public class VprasanjeVprasalnik {
    private int vprasanjeId;
    private int vprasalnikId;
    private boolean odgovor;    //odgovor na vprašanje z da/ne

    public VprasanjeVprasalnik(int vprasanjeId, int vprasalnikId, boolean odgovor) {
        this.vprasanjeId = vprasanjeId;
        this.vprasalnikId = vprasalnikId;
        this.odgovor = odgovor;
    }

    public int getVprasalnikId() {
        return vprasalnikId;
    }

    public void setVprasalnikId(int vprasalnikId) {
        this.vprasalnikId = vprasalnikId;
    }

    public int getVprasanjeId() {
        return vprasanjeId;
    }

    public void setVprasanjeId(int vprasanjeId) {
        this.vprasanjeId = vprasanjeId;
    }

    public boolean isOdgovor() {
        return odgovor;
    }

    public void setOdgovor(boolean odgovor) {
        this.odgovor = odgovor;
    }

    /**
     * Za vsako občutje napolnimo bazo s 4 testnimi primeri (kombinacijami odgovorov na
     * vprašanja) - možne kombinacije: (NE, NE, DA), (DA, NE, NE), (NE, DA, DA), (DA, NE, DA)
     * @return
     */
    public static VprasanjeVprasalnik[] populateData() {
        return new VprasanjeVprasalnik[] {
                new VprasanjeVprasalnik(1, 1, Boolean.FALSE),
                new VprasanjeVprasalnik(2, 1, Boolean.FALSE),
                new VprasanjeVprasalnik(3, 1, Boolean.TRUE),
                new VprasanjeVprasalnik(1, 2, Boolean.TRUE),
                new VprasanjeVprasalnik(2, 2, Boolean.FALSE),
                new VprasanjeVprasalnik(3, 2, Boolean.FALSE),
                new VprasanjeVprasalnik(1, 3, Boolean.FALSE),
                new VprasanjeVprasalnik(2, 3, Boolean.TRUE),
                new VprasanjeVprasalnik(3, 3, Boolean.TRUE),
                new VprasanjeVprasalnik(1, 4, Boolean.TRUE),
                new VprasanjeVprasalnik(2, 4, Boolean.TRUE),
                new VprasanjeVprasalnik(3, 4, Boolean.TRUE),

                new VprasanjeVprasalnik(1, 5, Boolean.FALSE),
                new VprasanjeVprasalnik(2, 5, Boolean.FALSE),
                new VprasanjeVprasalnik(3, 5, Boolean.TRUE),
                new VprasanjeVprasalnik(1, 6, Boolean.TRUE),
                new VprasanjeVprasalnik(2, 6, Boolean.FALSE),
                new VprasanjeVprasalnik(3, 6, Boolean.FALSE),
                new VprasanjeVprasalnik(1, 7, Boolean.FALSE),
                new VprasanjeVprasalnik(2, 7, Boolean.TRUE),
                new VprasanjeVprasalnik(3, 7, Boolean.TRUE),
                new VprasanjeVprasalnik(1, 8, Boolean.TRUE),
                new VprasanjeVprasalnik(2, 8, Boolean.TRUE),
                new VprasanjeVprasalnik(3, 8, Boolean.TRUE),

                new VprasanjeVprasalnik(1, 9, Boolean.FALSE),
                new VprasanjeVprasalnik(2, 9, Boolean.FALSE),
                new VprasanjeVprasalnik(3, 9, Boolean.TRUE),
                new VprasanjeVprasalnik(1, 10, Boolean.TRUE),
                new VprasanjeVprasalnik(2, 10, Boolean.FALSE),
                new VprasanjeVprasalnik(3, 10, Boolean.FALSE),
                new VprasanjeVprasalnik(1, 11, Boolean.FALSE),
                new VprasanjeVprasalnik(2, 11, Boolean.TRUE),
                new VprasanjeVprasalnik(3, 11, Boolean.TRUE),
                new VprasanjeVprasalnik(1, 12, Boolean.TRUE),
                new VprasanjeVprasalnik(2, 12, Boolean.TRUE),
                new VprasanjeVprasalnik(3, 12, Boolean.TRUE)
        };
    }
}
