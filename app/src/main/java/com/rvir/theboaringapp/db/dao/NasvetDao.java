package com.rvir.theboaringapp.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.rvir.theboaringapp.db.entitete.Nasvet;

import java.util.List;

@Dao
public interface NasvetDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long save(Nasvet nasvet);

    @Insert
    void insertAll(Nasvet... nasveti);

    @Update
    void update(Nasvet nasvet);

    @Delete
    void delete(Nasvet nasvet);

    @Query("SELECT * FROM Nasvet")
    LiveData<List<Nasvet>> findAll();

    @Query("SELECT * FROM Nasvet WHERE id = :id")
    Nasvet findOne(int id);
}
