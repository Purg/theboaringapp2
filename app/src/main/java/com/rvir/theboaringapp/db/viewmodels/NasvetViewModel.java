package com.rvir.theboaringapp.db.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.rvir.theboaringapp.db.MyDatabase;
import com.rvir.theboaringapp.db.dao.NasvetDao;
import com.rvir.theboaringapp.db.entitete.Nasvet;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NasvetViewModel extends AndroidViewModel {

    private NasvetDao nasvetDao;
    private ExecutorService executorService;

    public NasvetViewModel(@NonNull Application application) {
        super(application);
        nasvetDao = MyDatabase.getInstance(application).nasvetDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public Nasvet dobiNasvet(int id) {
        return nasvetDao.findOne(id);
    }

    public LiveData<List<Nasvet>> dobiNasvete() {
        return nasvetDao.findAll();
    }

    public void shraniNasvet(final Nasvet nasvet) {
        executorService.execute(() -> nasvetDao.save(nasvet));
    }

    public void zbrisiNasvet(final Nasvet nasvet) {
        executorService.execute(() -> nasvetDao.delete(nasvet));
    }
}
