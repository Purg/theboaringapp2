package com.rvir.theboaringapp.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.support.annotation.NonNull;

import com.rvir.theboaringapp.db.converters.NazivNasvetaTypeConverter;
import com.rvir.theboaringapp.db.dao.KategorijaNasvetDao;
import com.rvir.theboaringapp.db.dao.NasvetDao;
import com.rvir.theboaringapp.db.dao.ObcutjeDao;
import com.rvir.theboaringapp.db.dao.VprasalnikDao;
import com.rvir.theboaringapp.db.dao.VprasalnikNasvetDao;
import com.rvir.theboaringapp.db.dao.VprasanjeDao;
import com.rvir.theboaringapp.db.dao.VprasanjeVprasalnikDao;
import com.rvir.theboaringapp.db.entitete.KategorijaNasvet;
import com.rvir.theboaringapp.db.entitete.Nasvet;
import com.rvir.theboaringapp.db.entitete.Obcutje;
import com.rvir.theboaringapp.db.entitete.Vprasalnik;
import com.rvir.theboaringapp.db.entitete.VprasalnikNasvet;
import com.rvir.theboaringapp.db.entitete.Vprasanje;
import com.rvir.theboaringapp.db.entitete.VprasanjeVprasalnik;

import java.util.concurrent.Executors;

@Database(entities = {Obcutje.class, Vprasalnik.class, VprasanjeVprasalnik.class,
        Vprasanje.class, VprasalnikNasvet.class, Nasvet.class, KategorijaNasvet.class}, version = 1, exportSchema = false)
@TypeConverters({NazivNasvetaTypeConverter.class})
public abstract class MyDatabase extends RoomDatabase {

    private static MyDatabase INSTANCE;

    public abstract ObcutjeDao obcutjeDao();
    public abstract VprasalnikDao vprasalnikDao();
    public abstract VprasanjeVprasalnikDao vprasanjeVprasalnikDao();
    public abstract VprasanjeDao vprasanjeDao();
    public abstract VprasalnikNasvetDao vprasalnikNasvetDao();
    public abstract NasvetDao nasvetDao();
    public abstract KategorijaNasvetDao kategorijaNasvetDao();

    private static final Object sLock = new Object();

    public static MyDatabase getInstance(final Context context) {
        synchronized (sLock) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        MyDatabase.class, "TheBoaringApp.db")
                        .addCallback(new Callback() {
                            @Override
                            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                super.onCreate(db);
                                Executors.newSingleThreadScheduledExecutor().execute(() -> {
                                    getInstance(context).obcutjeDao().insertAll(Obcutje.populateData());
                                    getInstance(context).vprasalnikDao().insertAll(Vprasalnik.populateData());
                                    getInstance(context).vprasanjeDao().insertAll(Vprasanje.populateData());
                                    getInstance(context).kategorijaNasvetDao().insertAll(KategorijaNasvet.populateData());
                                    getInstance(context).nasvetDao().insertAll(Nasvet.populateData());
                                    getInstance(context).vprasanjeVprasalnikDao().insertAll(VprasanjeVprasalnik.populateData());
                                    getInstance(context).vprasalnikNasvetDao().insertAll(VprasalnikNasvet.populateData());
                                });
                            }
                        })
                        .build();
            }
            return INSTANCE;
        }
    }
}
