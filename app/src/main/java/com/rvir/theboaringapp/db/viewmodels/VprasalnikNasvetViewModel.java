package com.rvir.theboaringapp.db.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.rvir.theboaringapp.db.MyDatabase;
import com.rvir.theboaringapp.db.dao.VprasalnikNasvetDao;
import com.rvir.theboaringapp.db.entitete.VprasalnikNasvet;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VprasalnikNasvetViewModel extends AndroidViewModel {

    private VprasalnikNasvetDao vprasalnikNasvetDao;
    private ExecutorService executorService;

    public VprasalnikNasvetViewModel(@NonNull Application application) {
        super(application);
        vprasalnikNasvetDao = MyDatabase.getInstance(application).vprasalnikNasvetDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public ArrayList<VprasalnikNasvet> najdiNasvete(int vprasalnik) {
        return (ArrayList<VprasalnikNasvet>)vprasalnikNasvetDao.findVprasalnikNasvete(vprasalnik);
    }

    public void posodobiVprasalnikNasvet(VprasalnikNasvet vprasalnikNasvet) {
        executorService.execute(() -> vprasalnikNasvetDao.update(vprasalnikNasvet));
    }

    public LiveData<List<VprasalnikNasvet>> dobiVprasalnikNasvet() {
        return vprasalnikNasvetDao.findAll();
    }

    public VprasalnikNasvet dobiVprasalnikNasvet(int id, int id2) {
        return vprasalnikNasvetDao.findOne(id, id2);
    }

    public void shraniVprasalnikNasvet(final VprasalnikNasvet vprasalnikNasvet) {
        executorService.execute(() -> vprasalnikNasvetDao.save(vprasalnikNasvet));
    }

    public void zbrisiVprasalnikNasvet(final VprasalnikNasvet vprasalnikNasvet) {
        executorService.execute(() -> vprasalnikNasvetDao.delete(vprasalnikNasvet));
    }
}
