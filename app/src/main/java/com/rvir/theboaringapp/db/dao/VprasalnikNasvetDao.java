package com.rvir.theboaringapp.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.rvir.theboaringapp.db.entitete.VprasalnikNasvet;

import java.util.List;

@Dao
public interface VprasalnikNasvetDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long save(VprasalnikNasvet vprasalnikNasvet);

    @Insert
    void insertAll(VprasalnikNasvet... vprasalnikNasveti);

    @Update
    void update(VprasalnikNasvet vprasalnikNasvet);

    @Delete
    void delete(VprasalnikNasvet vprasalnikNasvet);

    @Query("SELECT * FROM VprasalnikNasvet")
    LiveData<List<VprasalnikNasvet>> findAll();

    @Query("SELECT * FROM VprasalnikNasvet WHERE vprasalnikId = :vprasalnikId and nasvetId = :nasvetId")
    VprasalnikNasvet findOne(int vprasalnikId, int nasvetId);

    @Query("SELECT DISTINCT VprasalnikNasvet.* FROM VprasalnikNasvet, Vprasalnik, Nasvet\n" +
            "WHERE Vprasalnik.id = :idVprasalnik AND VprasalnikNasvet.vprasalnikId = :idVprasalnik \n" +
            "AND VprasalnikNasvet.nasvetId IN (SELECT Nasvet.id FROM Nasvet, KategorijaNasvet\n" +
            "WHERE KategorijaNasvet.uporabnikovInteres = 1 AND KategorijaNasvet.id = Nasvet.kategorijaNasvetaId)")
    List<VprasalnikNasvet> findVprasalnikNasvete(int idVprasalnik);
}
