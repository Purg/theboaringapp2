package com.rvir.theboaringapp.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.rvir.theboaringapp.db.entitete.Obcutje;

import java.util.List;

@Dao
public interface ObcutjeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long save(Obcutje obcutje);

    @Insert
    void insertAll(Obcutje... obcutja);

    @Update
    void update(Obcutje obcutje);

    @Delete
    void delete(Obcutje obcutje);

    @Query("SELECT * FROM Obcutje")
    LiveData<List<Obcutje>> findAll();

    @Query("SELECT * FROM Obcutje WHERE id = :id")
    Obcutje findOne(int id);
}
