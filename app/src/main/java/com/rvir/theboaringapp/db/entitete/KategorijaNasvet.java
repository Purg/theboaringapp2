package com.rvir.theboaringapp.db.entitete;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * To je v bistvu interes uporabnika
 */
@Entity
public class KategorijaNasvet {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String naziv;
    private boolean uporabnikovInteres;

    public KategorijaNasvet(int id, String naziv, boolean uporabnikovInteres) {
        this.id = id;
        this.naziv = naziv;
        this.uporabnikovInteres = uporabnikovInteres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public boolean isUporabnikovInteres() {
        return uporabnikovInteres;
    }

    public void setUporabnikovInteres(boolean uporabnikovInteres) {
        this.uporabnikovInteres = uporabnikovInteres;
    }

    public static KategorijaNasvet[] populateData() {
        return new KategorijaNasvet[] {
                new KategorijaNasvet(0, "Skupinski športi", Boolean.FALSE), // nogomet, košarka, bowling
                new KategorijaNasvet(0, "Individualni športi", Boolean.FALSE),  // tek, fitnes
                new KategorijaNasvet(0, "Filmi/serije", Boolean.FALSE), // film, serija
                new KategorijaNasvet(0, "Sprostitev", Boolean.FALSE), // sprehod, toplice, meditacija, pijacka
                new KategorijaNasvet(0, "Kultura", Boolean.FALSE), // muzej, poslušanje glasbe, knjige
                new KategorijaNasvet(0, "Kreativnost", Boolean.FALSE), // kuhanje
        };
    }
}
