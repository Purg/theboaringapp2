package com.rvir.theboaringapp.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.rvir.theboaringapp.db.entitete.KategorijaNasvet;

import java.util.List;

@Dao
public interface KategorijaNasvetDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long save(KategorijaNasvet kategorijaNasvet);

    @Insert
    void insertAll(KategorijaNasvet... kategorijaNasveti);

    @Update
    void updateAll(KategorijaNasvet... kategorijaNasveti);

    @Update
    void update(KategorijaNasvet kategorijaNasvet);

    @Delete
    void delete(KategorijaNasvet kategorijaNasvet);

    @Query("SELECT * FROM KategorijaNasvet")
    List<KategorijaNasvet> findAll();

    @Query("SELECT * FROM KategorijaNasvet WHERE id = :id")
    KategorijaNasvet findOne(int id);
}
