package com.rvir.theboaringapp.db.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.rvir.theboaringapp.db.MyDatabase;
import com.rvir.theboaringapp.db.dao.VprasanjeVprasalnikDao;
import com.rvir.theboaringapp.db.entitete.VprasanjeVprasalnik;
import com.rvir.theboaringapp.db.entitete.poizvedbe.UjemajocVprasalnik;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VprasanjeVprasalnikViewModel extends AndroidViewModel {

    private VprasanjeVprasalnikDao vprasanjeVprasalnikDao;
    private ExecutorService executorService;

    public VprasanjeVprasalnikViewModel(@NonNull Application application) {
        super(application);
        vprasanjeVprasalnikDao = MyDatabase.getInstance(application).vprasanjeVprasalnikDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public UjemajocVprasalnik najdiVprasalnik(int obcutjeId, boolean odgovor1, boolean odgovor2, boolean odgovor3) {
        return vprasanjeVprasalnikDao.findVprasalnik(obcutjeId, odgovor1, odgovor2, odgovor3);
    }

    public LiveData<List<VprasanjeVprasalnik>> dobiVprasanjeVprasalnik() {
        return vprasanjeVprasalnikDao.findAll();
    }

    public VprasanjeVprasalnik dobiVprasanjeVprasalnik(int id, int id2) {
        return vprasanjeVprasalnikDao.findOne(id, id2);
    }

    public void shraniVprasanjeVprasalnik(final VprasanjeVprasalnik vprasanjeVprasalnik) {
        executorService.execute(() -> vprasanjeVprasalnikDao.save(vprasanjeVprasalnik));
    }

    public void zbrisiVprasanjeVprasalnik(final VprasanjeVprasalnik vprasanjeVprasalnik) {
        executorService.execute(() -> vprasanjeVprasalnikDao.delete(vprasanjeVprasalnik));
    }
}
