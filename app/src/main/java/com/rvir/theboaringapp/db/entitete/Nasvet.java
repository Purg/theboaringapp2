package com.rvir.theboaringapp.db.entitete;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.rvir.theboaringapp.enums.NazivNasveta;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = KategorijaNasvet.class,
        parentColumns = "id",
        childColumns = "kategorijaNasvetaId",
        onDelete = CASCADE), indices = {@Index(value = {"kategorijaNasvetaId"})})
public class Nasvet {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private NazivNasveta nazivNasveta;
    private String opis;
    private int kategorijaNasvetaId;

    public Nasvet(int id, NazivNasveta nazivNasveta, String opis, int kategorijaNasvetaId) {
        this.id = id;
        this.nazivNasveta = nazivNasveta;
        this.opis = opis;
        this.kategorijaNasvetaId = kategorijaNasvetaId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public NazivNasveta getNazivNasveta() {
        return nazivNasveta;
    }

    public void setNazivNasveta(NazivNasveta nazivNasveta) {
        this.nazivNasveta = nazivNasveta;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public int getKategorijaNasvetaId() {
        return kategorijaNasvetaId;
    }

    public void setKategorijaNasvetaId(int kategorijaNasvetaId) {
        this.kategorijaNasvetaId = kategorijaNasvetaId;
    }

    public static Nasvet[] populateData() {
        return new Nasvet[] {
                new Nasvet(0, NazivNasveta.NIC, "Predlagamo sprehod po mestu", 1),
                new Nasvet(0, NazivNasveta.FILM, "Predlagamo ogled filma", 3),
                new Nasvet(0, NazivNasveta.SERIJA, "Predlagamo ogled serije", 3),
                new Nasvet(0, NazivNasveta.KUHANJE, "Predlagamo kuhanje", 6),
                new Nasvet(0, NazivNasveta.GLASBA, "Predlagamo poslušanje glasbe", 5),
                new Nasvet(0, NazivNasveta.NOGOMET, "Predlagamo nogomet s prijatelji", 1),
                new Nasvet(0, NazivNasveta.KOSARKA, "Predlagamo košarko s prijatelji", 1),
                new Nasvet(0, NazivNasveta.BOWLING, "Predlagamo bowling s prijatelji", 1),
                new Nasvet(0, NazivNasveta.TEK, "Predlagamo tek na prostem", 2),
                new Nasvet(0, NazivNasveta.SPREHOD, "Predlagamo sprehod v parku", 4),
                new Nasvet(0, NazivNasveta.MUZEJ, "Predlagamo ogled muzeja", 5),
                new Nasvet(0, NazivNasveta.TOPLICE, "Predlagamo sprostitev v toplicah", 4),
                new Nasvet(0, NazivNasveta.FITNES, "Predlagamo trening v fitnesu", 2),
                new Nasvet(0, NazivNasveta.PAINTBALL, "Predlagamo paintball s prijatelji", 1),
                new Nasvet(0, NazivNasveta.TENIS, "Predlagamo tenis s prijatelji", 1),
                new Nasvet(0, NazivNasveta.PIJACKA, "Predlagamo druženje ob pijači s prijatelji", 1)
        };
    }
}
