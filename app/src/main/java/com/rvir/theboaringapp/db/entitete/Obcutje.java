package com.rvir.theboaringapp.db.entitete;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Obcutje {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String naziv;

    public Obcutje(int id, String naziv) {
        this.id = id;
        this.naziv = naziv;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public static Obcutje[] populateData() {
        return new Obcutje[] {
                new Obcutje(0, "Jeza"),
                new Obcutje(0, "Žalost"),
                new Obcutje(0, "Zaskrbljenost")
        };
    }
}
