package com.rvir.theboaringapp.db.entitete;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Obcutje.class,
        parentColumns = "id",
        childColumns = "obcutjeId",
        onDelete = CASCADE), indices = {@Index(value = {"obcutjeId"})})

public class Vprasalnik {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int obcutjeId;

    public Vprasalnik(int id, int obcutjeId) {
        this.id = id;
        this.obcutjeId = obcutjeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getObcutjeId() {
        return obcutjeId;
    }

    public void setObcutjeId(int obcutjeId) {
        this.obcutjeId = obcutjeId;
    }

    public static Vprasalnik[] populateData() {
        return new Vprasalnik[] {
                new Vprasalnik(0, 1),
                new Vprasalnik(0, 1),
                new Vprasalnik(0, 1),
                new Vprasalnik(0, 1),
                new Vprasalnik(0, 2),
                new Vprasalnik(0, 2),
                new Vprasalnik(0, 2),
                new Vprasalnik(0, 2),
                new Vprasalnik(0, 3),
                new Vprasalnik(0, 3),
                new Vprasalnik(0, 3),
                new Vprasalnik(0, 3)
        };
    }
}
