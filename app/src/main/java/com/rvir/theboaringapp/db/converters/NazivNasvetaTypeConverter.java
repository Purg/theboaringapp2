package com.rvir.theboaringapp.db.converters;

import android.arch.persistence.room.TypeConverter;

import com.rvir.theboaringapp.enums.NazivNasveta;


public class NazivNasvetaTypeConverter {

    @TypeConverter
    public static NazivNasveta toNazivNasveta(String vrsta) {
        for(NazivNasveta type : NazivNasveta.values())
            if(type.getVrsta().equals(vrsta))
                return type;

        return null;
    }

    @TypeConverter
    public static String toVrsta(NazivNasveta nazivNasveta) {
        return nazivNasveta == null ? null : nazivNasveta.getVrsta();
    }
}
