package com.rvir.theboaringapp.db.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.rvir.theboaringapp.db.MyDatabase;
import com.rvir.theboaringapp.db.dao.VprasanjeDao;
import com.rvir.theboaringapp.db.entitete.Vprasanje;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VprasanjeViewModel extends AndroidViewModel {

    private VprasanjeDao vprasanjeDao;
    private ExecutorService executorService;

    public VprasanjeViewModel(@NonNull Application application) {
        super(application);
        vprasanjeDao = MyDatabase.getInstance(application).vprasanjeDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public Vprasanje dobiObcutje(int id) {
        return vprasanjeDao.findOne(id);
    }

    public ArrayList<String> dobiVprasanja() {
        return (ArrayList<String>) vprasanjeDao.findAllList();
    }

    public void shraniObcutje(final Vprasanje obcutje) {
        executorService.execute(() -> vprasanjeDao.save(obcutje));
    }

    public void zbrisiObcutje(final Vprasanje obcutje) {
        executorService.execute(() -> vprasanjeDao.delete(obcutje));
    }
}
