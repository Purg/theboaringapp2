package com.rvir.theboaringapp.db.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.rvir.theboaringapp.db.MyDatabase;
import com.rvir.theboaringapp.db.dao.ObcutjeDao;
import com.rvir.theboaringapp.db.entitete.Obcutje;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ObcutjeViewModel extends AndroidViewModel {

    private ObcutjeDao obcutjeDao;
    private ExecutorService executorService;

    public ObcutjeViewModel(@NonNull Application application) {
        super(application);
        obcutjeDao = MyDatabase.getInstance(application).obcutjeDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public LiveData<List<Obcutje>> dobiObcutja() {
        return obcutjeDao.findAll();
    }

    public Obcutje dobiObcutje(int id) {
        return obcutjeDao.findOne(id);
    }

    public void shraniObcutje(final Obcutje obcutje) {
        executorService.execute(() -> obcutjeDao.save(obcutje));
    }

    public void zbrisiObcutje(final Obcutje obcutje) {
        executorService.execute(() -> obcutjeDao.delete(obcutje));
    }
}
