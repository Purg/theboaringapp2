package com.rvir.theboaringapp;

public interface IFragmentListener {

    void setFrag1Checked(int frag1Checked);

    void setFragPozicijaChecked(int fragVprasanjeChecked, int pozicija);

    void onNaprejButtonClicked();
}
