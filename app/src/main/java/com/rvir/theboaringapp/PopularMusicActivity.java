package com.rvir.theboaringapp;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.interesadapters.GlasbaRecyclerViewAdapter;
import com.rvir.theboaringapp.util.InteresAPIsHandler;
import com.rvir.theboaringapp.util.TopTrack;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;

public class PopularMusicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular_music);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayUseLogoEnabled(true);

        new PridobiGlasbo(this).execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_layout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.nastavitve:
                System.out.println("nastavitve klik");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private static class PridobiGlasbo extends AsyncTask<Void, Void, List<TopTrack>> {

        private WeakReference<PopularMusicActivity> activityReference;
        private ProgressDialog pDialog;

        PridobiGlasbo(PopularMusicActivity context) {
            activityReference = new WeakReference<>(context);
            pDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Prosimo počakajte...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected List<TopTrack> doInBackground(Void... arg0) {
            return new InteresAPIsHandler().getTopTracks();
        }

        @Override
        protected void onPostExecute(List<TopTrack> topTrackList) {

            PopularMusicActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            RecyclerView recyclerView = activity.findViewById(R.id.recyclerview_music_list_id);
            GlasbaRecyclerViewAdapter movieRecyclerViewAdapter = new GlasbaRecyclerViewAdapter(activity.getApplicationContext(), topTrackList);
            recyclerView.setLayoutManager(new GridLayoutManager(activity.getApplicationContext(), 2));
            recyclerView.setAdapter(movieRecyclerViewAdapter);
        }
    }
}
