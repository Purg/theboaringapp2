package com.rvir.theboaringapp;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.db.viewmodels.VprasanjeViewModel;

import java.util.List;

public class SplashScreenActivity extends AppCompatActivity {

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mHandler = new Handler();

        new Thread(() -> {
            List<String> vprasanja = ViewModelProviders.of(this).get(VprasanjeViewModel.class).dobiVprasanja(); // da napolnimo bazo ob prvem zagonu
            mHandler.postDelayed(() -> {

                SharedPreferences sharedPreferences  = getBaseContext().
                        getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

                boolean zeZagnano = sharedPreferences.getBoolean(getString(R.string.ze_zagnano), false);

                Intent i;
                if(!zeZagnano) {
                    i = new Intent(SplashScreenActivity.this, InteresiActivity.class);
                } else {
                    i = new Intent(SplashScreenActivity.this, MainActivity.class);
                }

                startActivity(i);

                finish();

            }, 2000);
        }).start();
    }
}
