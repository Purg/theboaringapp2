package com.rvir.theboaringapp;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.db.entitete.Obcutje;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Obcutje obcutje);
    }

    private List<Obcutje> mDataset;
    private OnItemClickListener listener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView naziv;

        public MyViewHolder(View v) {
            super(v);
            naziv = v.findViewById(R.id.obcutje_string);
        }

        public void bind(final Obcutje obcutje, final OnItemClickListener listener) {
            naziv.setText(obcutje.getNaziv());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(obcutje);
                }
            });
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(OnItemClickListener listener) {
        this.mDataset = new ArrayList<>();
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.obcutje_vrstica, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.bind(mDataset.get(position), listener);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setData(List<Obcutje> newData) {
        if (mDataset != null) {
            PostDiffCallback postDiffCallback = new PostDiffCallback(mDataset, newData);
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(postDiffCallback);

            mDataset.clear();
            mDataset.addAll(newData);
            diffResult.dispatchUpdatesTo(this);
        } else {
            // first initialization
            mDataset = newData;
        }
    }

    class PostDiffCallback extends DiffUtil.Callback {

        private final List<Obcutje> oldObcutje, newObcutje;

        public PostDiffCallback(List<Obcutje> oldObcutje, List<Obcutje> newObcutje) {
            this.oldObcutje = oldObcutje;
            this.newObcutje = newObcutje;
        }

        @Override
        public int getOldListSize() {
            return oldObcutje.size();
        }

        @Override
        public int getNewListSize() {
            return newObcutje.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldObcutje.get(oldItemPosition).getId() == newObcutje.get(newItemPosition).getId();
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldObcutje.get(oldItemPosition).equals(newObcutje.get(newItemPosition));
        }
    }
}
