package com.rvir.theboaringapp;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.db.entitete.Nasvet;
import com.rvir.theboaringapp.db.entitete.VprasalnikNasvet;
import com.rvir.theboaringapp.db.viewmodels.NasvetViewModel;
import com.rvir.theboaringapp.db.viewmodels.VprasalnikNasvetViewModel;
import com.rvir.theboaringapp.enums.NazivNasveta;
import com.rvir.theboaringapp.fragmenti.nasveti.CardViewNasvetFragment;
import com.rvir.theboaringapp.fragmenti.nasveti.GoogleMapsNasvetFragment;
import com.rvir.theboaringapp.fragmenti.nasveti.ReceptNasvetFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class NasvetActivity extends AppCompatActivity {

    private final static String TAG = "NasvetActivity";

    private VprasalnikNasvetViewModel vprasalnikNasvetViewModel;
    private NasvetViewModel nasvetViewModel;
    private Handler mHandler;

    private int vprasalnikId;
    private Nasvet trenutniNasvet;
    private VprasalnikNasvet trenutniVprasalnikNasvet;
    private List<VprasalnikNasvet> vprasalnikNasvetList;

    private List<VprasalnikNasvet> dobriNevtralniNasveti;

    private TextView opisNasveta;
    private RadioGroup radioLikeDislike;
    private TextView vsec_vprasanje;

    /**
     * pokliče query, ki poišče vse možne nasvete in jih sortira v dva lista -
     * dobri oz. lajkani nasveti in nevtralni oz. nasveti brez podanega mnenja uporabnika
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nasvet);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        vprasalnikId = getIntent().getIntExtra("vprasalnikId", 0);
        Log.d(TAG, "vprasalnikId: " + vprasalnikId);

        opisNasveta = findViewById(R.id.nasvet_opis);
        radioLikeDislike = findViewById(R.id.radio_like_group);
        vsec_vprasanje = findViewById(R.id.text_vsec_nasvet);

        vprasalnikNasvetViewModel = ViewModelProviders.of(this).get(VprasalnikNasvetViewModel.class);
        nasvetViewModel = ViewModelProviders.of(this).get(NasvetViewModel.class);

        mHandler = new Handler();
        new Thread(() -> {
            vprasalnikNasvetList = vprasalnikNasvetViewModel.najdiNasvete(vprasalnikId);
            Log.d(TAG, "velikost: " + vprasalnikNasvetList.size());
            mHandler.post(() -> {
                dobriNevtralniNasveti = new ArrayList<>();
                for (VprasalnikNasvet vn : vprasalnikNasvetList) {
                    if (vn.isDoberNasvet() == null || (vn.isDoberNasvet() != null && vn.isDoberNasvet())) {
                        dobriNevtralniNasveti.add(new VprasalnikNasvet(vn.getVprasalnikId(),
                                vn.getNasvetId(), vn.isDoberNasvet()));
                    }
                }

                radioLikeDislike.setOnCheckedChangeListener((group, checkedId) -> {
                    Log.d(TAG, "prožim setOnCheckedChangeListener");
                    Boolean doberNasvet = trenutniVprasalnikNasvet.isDoberNasvet();
                    switch (checkedId) {
                        case R.id.radio_like:
                            if(doberNasvet != null && doberNasvet) {
                                break;
                            } else {
                                trenutniVprasalnikNasvet.setDoberNasvet(Boolean.TRUE);
                                vprasalnikNasvetViewModel.posodobiVprasalnikNasvet(trenutniVprasalnikNasvet);
                                Toast.makeText(this, "Izbira zabeležena: Dober nasvet", Toast.LENGTH_SHORT).show();
                                break;
                            }
                        case R.id.radio_dislike:
                            if(doberNasvet != null && !doberNasvet) {
                                break;
                            } else {
                                trenutniVprasalnikNasvet.setDoberNasvet(Boolean.FALSE);
                                vprasalnikNasvetViewModel.posodobiVprasalnikNasvet(trenutniVprasalnikNasvet);
                                Toast.makeText(this, "Izbira zabeležena: Slab nasvet", Toast.LENGTH_SHORT).show();
                                break;
                            }
                    }
                });

                nastaviNasvet();
            });
        }).start();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_layout_nasvet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.swap:
                Log.d(TAG, "swap klik, velikost: " + dobriNevtralniNasveti.size());
                if (trenutniNasvet.getId() != 1) {
                    dobriNevtralniNasveti.removeIf(vprasalnikNasvet ->
                            vprasalnikNasvet.equals(trenutniVprasalnikNasvet));
                    Log.d(TAG, "nova velikost: " + dobriNevtralniNasveti.size());
                    radioLikeDislike.clearCheck();
                    nastaviNasvet();
                } else {
                    Toast.makeText(this, "Na voljo ni več novih nasvetov.", Toast.LENGTH_SHORT).show();
                    item.setEnabled(false);
                }
                return true;
            case R.id.domov:
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * iz lista(najprej pogleda med lajkanimi nasveti, nato med tistimi, ki niso ne lajkanim, ne dislajkani)
     * izbere random nasvet in ga prikaže na pripadajočem fragmentu
     */
    private void nastaviNasvet() {
        int nasvetId;

        if(dobriNevtralniNasveti != null && !dobriNevtralniNasveti.isEmpty()) {
            VprasalnikNasvet vn = dobriNevtralniNasveti.get(new Random().nextInt(dobriNevtralniNasveti.size()));
            trenutniVprasalnikNasvet = vn;
            nasvetId = vn.getNasvetId();
            Log.d(TAG, "nasvetId: " + nasvetId);
        } else {
            nasvetId = 1;
        }

        new Thread(() -> {
            trenutniNasvet = nasvetViewModel.dobiNasvet(nasvetId);
            mHandler.post(() -> {


                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container_nasveti, najdiFragment());
                ft.commit();

                String opisNasvetString = "";

                switch (trenutniNasvet.getNazivNasveta()){
                    case NIC:
                        opisNasvetString = getString(R.string.nic);
                        break;

                    case FILM:
                        opisNasvetString = getString(R.string.filmNasvet);
                        break;

                    case SERIJA:
                        opisNasvetString = getString(R.string.serijaNasvet);
                        break;

                    case KUHANJE:
                        opisNasvetString = getString(R.string.kuhinjaNasvet);
                        break;

                    case GLASBA:
                        opisNasvetString = getString(R.string.glasbaNasvet);
                        break;

                    case NOGOMET:
                        opisNasvetString = getString(R.string.nogometNasvet);
                        break;

                    case KOSARKA:
                        opisNasvetString = getString(R.string.kosarkaNasvet);
                        break;

                    case BOWLING:
                        opisNasvetString = getString(R.string.bowlingNasvet);
                        break;

                    case TEK:
                        opisNasvetString = getString(R.string.tekNasvet);
                        break;

                    case SPREHOD:
                        opisNasvetString = getString(R.string.sprehodNasvet);
                        break;

                    case MUZEJ:
                        opisNasvetString = getString(R.string.muzejNasvet);
                        break;

                    case TOPLICE:
                        opisNasvetString = getString(R.string.topliceNasvet);
                        break;

                    case FITNES:
                        opisNasvetString = getString(R.string.fitnesNasvet);
                        break;

                    case PAINTBALL:
                        opisNasvetString = getString(R.string.paintballNasvet);
                        break;

                    case TENIS:
                        opisNasvetString = getString(R.string.tenisNasvet);
                        break;

                    case PIJACKA:
                        opisNasvetString = getString(R.string.pijacaNasvet);
                        break;
                }


                opisNasveta.setText(opisNasvetString);

                if(trenutniVprasalnikNasvet != null && trenutniVprasalnikNasvet.isDoberNasvet() != null) {
                    if(trenutniVprasalnikNasvet.isDoberNasvet()) {
                        radioLikeDislike.getChildAt(1).setActivated(true);
                    } else {
                        radioLikeDislike.getChildAt(0).setActivated(true);
                    }
                }

                if(trenutniNasvet.getId() == 1) {
                    vsec_vprasanje.setVisibility(View.GONE);
                    radioLikeDislike.setVisibility(View.GONE);
                }

            });
        }).start();
    }

    /**
     * poišče fragment za prikaz nasveta, glede na naziv nasveta
     * @return fragment za prikaz
     */
    private Fragment najdiFragment() {
        NazivNasveta naziv = trenutniNasvet.getNazivNasveta();
        if((naziv.equals(NazivNasveta.FILM)) || (naziv.equals(NazivNasveta.SERIJA))
                || (naziv.equals(NazivNasveta.GLASBA))) {
            return CardViewNasvetFragment.newInstance(trenutniNasvet);
        } else if(naziv.equals(NazivNasveta.KUHANJE)) {
            return ReceptNasvetFragment.newInstance();
        } else {
            return GoogleMapsNasvetFragment.newInstance(trenutniNasvet);
        }
    }
}
