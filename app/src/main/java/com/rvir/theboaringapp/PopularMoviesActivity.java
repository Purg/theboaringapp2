package com.rvir.theboaringapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.interesadapters.MovieRecyclerViewAdapter;
import com.rvir.theboaringapp.util.InteresAPIsHandler;
import com.rvir.theboaringapp.util.PopularniFilmiSerije;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;

public class PopularMoviesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular_movies);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayUseLogoEnabled(true);

        new PridobiFilme(this).execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_layout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.nastavitve:
                System.out.println("nastavitve klik");
                Intent ii = new Intent(this, InteresiActivity.class);
                startActivity(ii);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private static class PridobiFilme extends AsyncTask<Void, Void, List<PopularniFilmiSerije>> {

        private WeakReference<PopularMoviesActivity> activityReference;
        private ProgressDialog pDialog;

        PridobiFilme(PopularMoviesActivity context) {
            activityReference = new WeakReference<>(context);
            pDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Prosimo počakajte...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected List<PopularniFilmiSerije> doInBackground(Void... arg0) {
            return new InteresAPIsHandler().getPopularMovies("https://api.themoviedb.org/3/movie/popular?api_key=2911b5389a356b51c23c907f6c29d609&language=en-US&page=1");
        }

        @Override
        protected void onPostExecute(List<PopularniFilmiSerije> popularniFilmiSerijeList) {

            PopularMoviesActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            RecyclerView recyclerView = activity.findViewById(R.id.recyclerview_movie_list_id);
            MovieRecyclerViewAdapter movieRecyclerViewAdapter = new MovieRecyclerViewAdapter(activity.getApplicationContext(), popularniFilmiSerijeList);
            recyclerView.setLayoutManager(new GridLayoutManager(activity.getApplicationContext(), 2));
            recyclerView.setAdapter(movieRecyclerViewAdapter);
        }
    }
}
