package com.rvir.theboaringapp.places;

import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;

public class GetNearbyPlaces extends AsyncTask<Object, String, String> {
    private String googleplaceData, url;
    private GoogleMap mMap;
    private WeakReference<FragmentActivity> activityReference;

    public GetNearbyPlaces(FragmentActivity fragmentActivity) {
        activityReference = new WeakReference<>(fragmentActivity);
    }

    @Override
    protected String doInBackground(Object... objects) {
        mMap = (GoogleMap) objects[0];
        url = (String) objects[1];

        DownloadUrl downloadUrl = new DownloadUrl();
        try {
            googleplaceData = downloadUrl.ReadTheURL(url);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return googleplaceData;
    }


    @Override
    protected void onPostExecute(String s) {
        List<HashMap<String, String>> nearByPlacesList = null;
        DataParser dataParser = new DataParser();
        nearByPlacesList = dataParser.parse(s);

        displayNearbyPlaces(nearByPlacesList);
    }


    private void displayNearbyPlaces(List<HashMap<String, String>> nearByPlacesList) {
        if (nearByPlacesList == null) {
            Toast.makeText(activityReference.get(), "Napaka pri pridobivanju podatkov", Toast.LENGTH_SHORT).show();
        } else {
            for (int i = 0; i < nearByPlacesList.size(); i++) {
                MarkerOptions markerOptions = new MarkerOptions();

                HashMap<String, String> googleNearbyPlace = nearByPlacesList.get(i);
                String nameOfPlace = googleNearbyPlace.get("place_name");
//            String vicinity = googleNearbyPlace.get("vicinity");
                double lat = Double.parseDouble(googleNearbyPlace.get("lat"));
                double lng = Double.parseDouble(googleNearbyPlace.get("lng"));

                mMap.addMarker(new MarkerOptions()
                        .title(nameOfPlace)
                        .position(new LatLng(lat, lng))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
            }

            mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        }
    }
}
