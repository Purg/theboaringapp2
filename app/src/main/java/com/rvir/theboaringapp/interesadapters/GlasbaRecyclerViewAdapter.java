package com.rvir.theboaringapp.interesadapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.util.TopTrack;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GlasbaRecyclerViewAdapter extends RecyclerView.Adapter<GlasbaRecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private List<TopTrack> mList;

    public GlasbaRecyclerViewAdapter(Context context, List<TopTrack> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        view = layoutInflater.inflate(R.layout.cardview_item_glasba, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.glasbaNaslov.setText(mList.get(position).getNaslov());
        holder.glasbaIzvajalec.setText(mList.get(position).getIzvajalec());
        Picasso.get().load(mList.get(position).getLinkDoSlike()).into(holder.glasbaImage);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView glasbaImage;
        TextView glasbaNaslov;
        TextView glasbaIzvajalec;

        public MyViewHolder(View itemView) {
            super(itemView);

            glasbaImage = itemView.findViewById(R.id.glasba_image);
            glasbaNaslov = itemView.findViewById(R.id.glasba_naslov);
            glasbaIzvajalec = itemView.findViewById(R.id.glasba_izvajalec);
        }
    }
}
