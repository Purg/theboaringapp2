package com.rvir.theboaringapp;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.db.entitete.KategorijaNasvet;
import com.rvir.theboaringapp.db.viewmodels.KategorijaNasvetViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class InteresiActivity extends AppCompatActivity {

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private KategorijaNasvetViewModel kategorijaNasvetViewModel;
    private List<KategorijaNasvet> kategorijaNasveti;
    private List<CheckBox> checkBoxList = new ArrayList<>();
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.interesi_activity);

        SharedPreferences sharedPreferences = getBaseContext().
                getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        boolean zeZagnano = sharedPreferences.getBoolean(getString(R.string.ze_zagnano), false);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(zeZagnano);
        getSupportActionBar().setDisplayHomeAsUpEnabled(zeZagnano);

        LinearLayout lo = findViewById(R.id.checkboxContainer);

        Button shrani = findViewById(R.id.button);
        TextView pozdravInteresi = findViewById(R.id.pozdrav_interesi);
        TextView textInteresi = findViewById(R.id.text_interesi);

        if (zeZagnano) {
            pozdravInteresi.setText(getResources().getString(R.string.urejanje_interesov));
            textInteresi.setText(getResources().getString(R.string.urejanje_interesov_text));
        }

        kategorijaNasvetViewModel = ViewModelProviders.of(this).get(KategorijaNasvetViewModel.class);

        getLocationPermission();

        mHandler = new Handler();
        new Thread(() -> {
            kategorijaNasveti = kategorijaNasvetViewModel.dobiInterese();
            Log.d("kategorijaNasveti", "velikost: " + kategorijaNasveti.size());
            mHandler.post(() -> {
                for (KategorijaNasvet kategorijaNasvet : kategorijaNasveti) {
                    CheckBox cb = new CheckBox(getApplicationContext());

                    String kategorijaNasvetString = "";

                    switch ( kategorijaNasvet.getNaziv()){
                        case "Skupinski športi":
                            kategorijaNasvetString = getString(R.string.skupinskiSporti);
                            break;

                        case "Individualni športi":
                            kategorijaNasvetString = getString(R.string.indiSporti);
                            break;

                        case "Filmi/serije":
                            kategorijaNasvetString = getString(R.string.filmiSerije);
                            break;

                        case "Sprostitev":
                            kategorijaNasvetString = getString(R.string.sprostitev);
                            break;

                        case  "Kultura":
                            kategorijaNasvetString = getString(R.string.kultura);
                            break;

                        case "Kreativnost":
                            kategorijaNasvetString = getString(R.string.kreativnost);
                            break;


                    }

                    cb.setText(kategorijaNasvetString);
                    cb.setTypeface(ResourcesCompat.getFont(this, R.font.liberty_d_family));
                    cb.setTextColor(Color.parseColor("#545454"));
                    cb.setLayoutParams(getLayoutParamsCheckbox());
                    if (zeZagnano) {
                        cb.setChecked(kategorijaNasvet.isUporabnikovInteres());
                    }

                    checkBoxList.add(cb);
                    lo.addView(cb);
                }
            });
        }).start();

        shrani.setOnClickListener(view -> {
            for (int i = 0; i < checkBoxList.size(); i++) {
                kategorijaNasveti.get(i).setUporabnikovInteres(checkBoxList.get(i).isChecked());
                Log.d("interesiActivity", "vrednost: " + checkBoxList.get(i).isChecked());
            }

            Toast.makeText(this, "Interesi uspešno shranjeni", Toast.LENGTH_SHORT).show();

            kategorijaNasvetViewModel.posodobiInterese(kategorijaNasveti);

            if (!zeZagnano) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putBoolean(getString(R.string.ze_zagnano), Boolean.TRUE);
                edit.apply();
            }

            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        });

    }

    private LayoutParams getLayoutParamsCheckbox() {
        LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, 15);
        return layoutParams;
    }

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        } else {
            Log.d("asdf", "sem sel v else");
        }
    }
}
