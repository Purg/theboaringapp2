package com.rvir.theboaringapp.util;

import java.util.ArrayList;
import java.util.List;

public class KuharskiRecept {

    private String naziv;
    private String linkDoSlike;
    private ArrayList<String> sestavine;
    private String casPriprave;
    private String calories;
    private String teza;

    public KuharskiRecept(String naziv, String linkDoSlike, ArrayList<String> sestavine, String casPriprave, String calories, String teza) {
        this.naziv = naziv;
        this.linkDoSlike = linkDoSlike;
        this.sestavine = sestavine;
        this.casPriprave = casPriprave;
        this.calories = calories;
        this.teza = teza;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<String> getSestavine() {
        return sestavine;
    }

    public void setSestavine(ArrayList<String> sestavine) {
        this.sestavine = sestavine;
    }

    public String getCasPriprave() {
        return casPriprave;
    }

    public void setCasPriprave(String casPriprave) {
        this.casPriprave = casPriprave;
    }

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public String getTeza() { return teza; }

    public void setTeza(String teza) { this.teza = teza; }

    public String getLinkDoSlike() { return linkDoSlike; }

    public void setLinkDoSlike(String linkDoSlike) {
        this.linkDoSlike = linkDoSlike;
    }
}
