package com.rvir.theboaringapp.util;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Razred služi vsem nasvetom, ki potrebujejo informacije iz neke spletne storitve
 * To so zaenkrat lahko: šale, citati in kuharski recepti
 */
public class InteresAPIsHandler {

    private String[] kljucneBesede = new String[5];

    public InteresAPIsHandler() {}

    /**
     * Pridobi citat iz api-ja
     * @return String quote, ki je lahko dejanski citat, sestavljen iz citata in avtorja v HTML-ju, lahko pa je tudi niz sporočila o napaki
     */
    public String getQuoteFromApi() {
        String quote = "Prišlo je do težave pri pridobivanju citata";
        String author = "";

        HttpHandler hh = new HttpHandler();
        try {
            JSONArray quoteObject = new JSONArray(hh.makeServiceCall(
                    "http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1"));

            quote = quoteObject.getJSONObject(0).getString("content");
            author = "\n - " + quoteObject.getJSONObject(0).getString("title");
            quote += author;


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return quote;
    }

    /**
     * Pridobi kuharski recept iz api-ja
     * @return nov kuharski recept sestavljen iz pridoblenih podatkov
     * Pridobijo se: naziv, sestavine ter link do celotnega recepta
     */
    public KuharskiRecept getRecipeFromApi() {

        napolniKljucneBesede();

        String naziv = null;
        ArrayList<String> sestavine = new ArrayList<>();
        String casPriprave = null;
        String linkDoSlike = null;
        double calories = 0d;
        double teza = 0d;

        HttpHandler hh = new HttpHandler();

        try {
            JSONObject recipesObject = new JSONObject(hh.makeServiceCall(
                "https://api.edamam.com/search?app_id=98d4360d&app_key=1e168b4008e6b77ad33a7ba639840168&to=100&q=" + getNakljucnaKljucnaBeseda()));

            JSONArray hits = recipesObject.getJSONArray("hits");
            int randomIntIndex = new Random().nextInt(hits.length());
            JSONObject recipe = hits.getJSONObject(randomIntIndex).getJSONObject("recipe");

            naziv = recipe.getString("label");

            JSONArray ingredientLinesArray = recipe.getJSONArray("ingredientLines");
            int ingredientLength = ingredientLinesArray.length();

            for (int i = 0; i < ingredientLength; i++) {
                sestavine.add(ingredientLinesArray.getString(i));
            }

            linkDoSlike = recipe.getString("image");
            casPriprave = recipe.getString("totalTime");
            calories = Double.parseDouble(recipe.getString("calories"));
            teza = Double.parseDouble(recipe.getString("totalWeight"));

            if (naziv == null || sestavine.isEmpty() || casPriprave == null || calories == 0 || teza == 0 || linkDoSlike == null) {
                return null;
            } else {
                return new KuharskiRecept(naziv, linkDoSlike, sestavine, casPriprave + " min", String.format("%.0f", calories) + " kcal", String.format("%.0f", teza) + " g");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<PopularniFilmiSerije> getPopularMovies(String url) {

        List<PopularniFilmiSerije> popularniFilmiSerijes = new ArrayList<>();

        HttpHandler hh = new HttpHandler();

        try {
            JSONObject moviesObject = null;

            moviesObject = new JSONObject(hh.makeServiceCall(
                    url));

            JSONArray allMovies = moviesObject.getJSONArray("results");

            for (int i = 0; i < 10; i++) {
                JSONObject currentMovie = allMovies.getJSONObject(i);
                String naslov;
                try {
                    naslov = currentMovie.getString("title");
                } catch (JSONException ex) {
                    Log.d("moviesException", "napaka: " + ex.getMessage());
                    naslov = currentMovie.getString("name");
                }
                Log.d("popularMovies", "naslov:" + naslov);
                String linkDoSlike = currentMovie.getString("poster_path");

                if (naslov == null || linkDoSlike == null) {
                    return null;
                } else {
                    linkDoSlike = "http://image.tmdb.org/t/p/w185" + linkDoSlike;
                    popularniFilmiSerijes.add(new PopularniFilmiSerije(naslov, linkDoSlike));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return popularniFilmiSerijes;
    }

    public List<TopTrack> getTopTracks() {

        List<TopTrack> topTracks = new ArrayList<>();

        HttpHandler hh = new HttpHandler();

        try {
            JSONObject topTracksObject = null;

            topTracksObject = new JSONObject(hh.makeServiceCall(
                    "http://ws.audioscrobbler.com/2.0/?method=chart.getTopTracks&api_key=12eed98f620b02fbc2de23189014a86d&format=json&limit=10"));

            JSONArray allTopTracks = topTracksObject.getJSONObject("tracks").getJSONArray("track");

            for (int i = 0; i < allTopTracks.length(); i++) {
                JSONObject currentTopTrack = allTopTracks.getJSONObject(i);
                String linkDoSlike = currentTopTrack.getJSONArray("image").getJSONObject(2).getString("#text");
                String naslov = currentTopTrack.getString("name");
                String izvajalec = currentTopTrack.getJSONObject("artist").getString("name");

                Log.d("topTracks", "slika: " + linkDoSlike + ", naslov: " + naslov + ", izvajalec: " + izvajalec);

                if (naslov == null || linkDoSlike == null) {
                    return null;
                } else {
                    topTracks.add(new TopTrack(linkDoSlike, naslov, izvajalec));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return topTracks;
    }

    public void napolniKljucneBesede() {
        this.kljucneBesede[0] = "meat";
        this.kljucneBesede[1] = "vegetarian";
        this.kljucneBesede[2] = "dessert";
        this.kljucneBesede[3] = "vegan";
        this.kljucneBesede[4] = "salat";
    }

    public String getNakljucnaKljucnaBeseda() {
        return kljucneBesede[new Random().nextInt(5)];
    }
}
