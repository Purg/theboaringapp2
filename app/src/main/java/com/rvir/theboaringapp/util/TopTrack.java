package com.rvir.theboaringapp.util;

public class TopTrack {

    private String linkDoSlike;
    private String naslov;
    private String izvajalec;

    public TopTrack(String linkDoSlike, String naslov, String izvajalec) {
        this.linkDoSlike = linkDoSlike;
        this.naslov = naslov;
        this.izvajalec = izvajalec;
    }

    public String getLinkDoSlike() {
        return linkDoSlike;
    }

    public void setLinkDoSlike(String linkDoSlike) {
        this.linkDoSlike = linkDoSlike;
    }

    public String getNaslov() {
        return naslov;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }

    public String getIzvajalec() {
        return izvajalec;
    }

    public void setIzvajalec(String izvajalec) {
        this.izvajalec = izvajalec;
    }
}
