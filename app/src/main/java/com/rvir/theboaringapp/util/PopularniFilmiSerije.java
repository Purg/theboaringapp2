package com.rvir.theboaringapp.util;

public class PopularniFilmiSerije {

    private String naslov;
    private String linkDoSlike;

    public PopularniFilmiSerije(String naslov, String linkDoSlike) {
        this.naslov = naslov;
        this.linkDoSlike = linkDoSlike;
    }

    public String getNaslov() {
        return naslov;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }

    public String getLinkDoSlike() {
        return linkDoSlike;
    }

    public void setLinkDoSlike(String linkDoSlike) {
        this.linkDoSlike = linkDoSlike;
    }
}
