package com.rvir.theboaringapp.util;

public class MovieSeries {
    private String naslov;
    private String datumIzdaje;
    private String zvrst;
    private String trajanje;
    private String plot;
    private double imdbOcena;
    private String linkDoSlike;

    public MovieSeries(String naslov, String datumIzdaje, String zvrst, String trajanje, String plot, double imdbOcena, String linkDoSlike) {
        this.naslov = naslov;
        this.datumIzdaje = datumIzdaje;
        this.zvrst = zvrst;
        this.trajanje = trajanje;
        this.plot = plot;
        this.imdbOcena = imdbOcena;
        this.linkDoSlike = linkDoSlike;
    }

    public String getNaslov() {
        return naslov;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }

    public String getDatumIzdaje() {
        return datumIzdaje;
    }

    public void setDatumIzdaje(String datumIzdaje) {
        this.datumIzdaje = datumIzdaje;
    }

    public String getZvrst() {
        return zvrst;
    }

    public void setZvrst(String zvrst) {
        this.zvrst = zvrst;
    }

    public String getTrajanje() {
        return trajanje;
    }

    public void setTrajanje(String trajanje) {
        this.trajanje = trajanje;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public double getImdbOcena() {
        return imdbOcena;
    }

    public void setImdbOcena(double imdbOcena) {
        this.imdbOcena = imdbOcena;
    }

    public String getLinkDoSlike() {
        return linkDoSlike;
    }

    public void setLinkDoSlike(String linkDoSlike) {
        this.linkDoSlike = linkDoSlike;
    }
}
