package com.rvir.theboaringapp;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.db.entitete.poizvedbe.UjemajocVprasalnik;
import com.rvir.theboaringapp.db.viewmodels.VprasanjeViewModel;
import com.rvir.theboaringapp.db.viewmodels.VprasanjeVprasalnikViewModel;
import com.rvir.theboaringapp.fragmenti.vprasalnik.VprasanjeFragment;
import com.rvir.theboaringapp.fragmenti.vprasalnik.VprasanjeObcutjeFragment;

import java.util.List;
import java.util.Objects;

public class VprasalnikActivity extends AppCompatActivity implements IFragmentListener {

    private FragmentPagerAdapter adapterViewPager;
    private VprasanjeViewModel vprasanjeViewModel;
    private VprasanjeVprasalnikViewModel vprasanjeVprasalnikViewModel;
    private ViewPager vpPager;
    private List<String> vprasanja;
    private Handler mHandler;


    public int frag1Checked = -1;
    private int frag2Checked = -1;
    private int frag3Checked = -1;
    private int frag4Checked = -1;
    private UjemajocVprasalnik vprasalnik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vprasalnik_activity);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        vprasanjeViewModel = ViewModelProviders.of(this).get(VprasanjeViewModel.class);
        vprasanjeVprasalnikViewModel = ViewModelProviders.of(this).get(VprasanjeVprasalnikViewModel.class);

        vpPager = findViewById(R.id.view_pager_vprasalnik);

        mHandler = new Handler();
        new Thread(() -> {
            vprasanja = vprasanjeViewModel.dobiVprasanja();
            mHandler.post(() -> {
                adapterViewPager = new MyPagerAdapter(getSupportFragmentManager(), vprasanja);
                vpPager.setAdapter(adapterViewPager);
            });
        }).start();
    }

    public void setFrag1Checked(int frag1Checked) {
        if(this.frag1Checked != frag1Checked) {
            this.frag1Checked = frag1Checked;
            vpPager.setCurrentItem(1, true);
        }
    }

    public void setFragPozicijaChecked(int fragPozicijaChecked, int pozicija) {
        switch (pozicija) {
            case 1:
                if(this.frag2Checked != fragPozicijaChecked) {
                    this.frag2Checked = fragPozicijaChecked;
                    vpPager.setCurrentItem(2, true);
                }
                break;
            case 2:
                if(this.frag3Checked != fragPozicijaChecked) {
                    this.frag3Checked = fragPozicijaChecked;
                    vpPager.setCurrentItem(3, true);
                }
                break;
            case 3:
                if(this.frag4Checked != fragPozicijaChecked) {
                    this.frag4Checked = fragPozicijaChecked;
                }
                break;
        }
    }

    public void onNaprejButtonClicked() {
        Log.i("naprej clicked", "vrednosti: " + frag1Checked + ", " + frag2Checked + ", " + frag3Checked + ", " + frag4Checked);
        mHandler = new Handler();
        new Thread(() -> {
            vprasalnik = vprasanjeVprasalnikViewModel.najdiVprasalnik(frag1Checked, frag2Checked==1, frag3Checked==1, frag4Checked==1);
            Log.d("VpraslnikActivity", "vprasalnikId: " + vprasalnik.getVprasalnikId());
            mHandler.post(() -> {
                Intent i = new Intent(this, NasvetActivity.class);
                i.putExtra("vprasalnikId", vprasalnik.getVprasalnikId());
                startActivity(i);
            });
        }).start();
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 4;
        private List<String> vprasnja;

        MyPagerAdapter(FragmentManager fragmentManager, List<String> vprasanja) {
            super(fragmentManager);
            this.vprasnja = vprasanja;
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            if(position == 0) {
                return new VprasanjeObcutjeFragment();
            } else if(position <= 3) {
                return VprasanjeFragment.newInstance(vprasnja.get(position-1), position);
            }
            return null;
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }
}
