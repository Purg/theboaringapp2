package com.rvir.theboaringapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rvir.theboaringapp.R;
import com.rvir.theboaringapp.util.InteresAPIsHandler;
import com.rvir.theboaringapp.util.KuharskiRecept;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

public class CookingRecipeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recept_nasvet_fragment);

        new PridobiKuharskiRecept(this).execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.nastavitve:
                System.out.println("nastavitve klik");
                Intent ii = new Intent(this, InteresiActivity.class);
                startActivity(ii);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private static class PridobiKuharskiRecept extends AsyncTask<Void, Void, KuharskiRecept> {

        private WeakReference<CookingRecipeActivity> activityReference;
        private ProgressDialog pDialog;

        PridobiKuharskiRecept(CookingRecipeActivity context) {
            activityReference = new WeakReference<>(context);
            pDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Prosimo počakajte...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected KuharskiRecept doInBackground(Void... arg0) {
            return new InteresAPIsHandler().getRecipeFromApi();
        }

        @Override
        protected void onPostExecute(KuharskiRecept kuharskiRecept) {

            CookingRecipeActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            ImageView image = activity.findViewById(R.id.recipe_image);
            Picasso.get().load(kuharskiRecept.getLinkDoSlike()).into(image);

            TextView title = activity.findViewById(R.id.title);
            title.setText(kuharskiRecept.getNaziv());

            TextView time = activity.findViewById(R.id.time);
            time.setText(kuharskiRecept.getCasPriprave());

            TextView calories = activity.findViewById(R.id.calories);
            calories.setText(kuharskiRecept.getCalories());

            TextView weight = activity.findViewById(R.id.weight);
            weight.setText(kuharskiRecept.getTeza());

            ListView sestavine = activity.findViewById(R.id.sestavine);
            ArrayAdapter adapter = new ArrayAdapter<>(activity.getApplicationContext(), android.R.layout.simple_list_item_1, kuharskiRecept.getSestavine());
            sestavine.setAdapter(adapter);

        }
    }
}
