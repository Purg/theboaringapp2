package com.rvir.theboaringapp;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.rvir.theboaringapp.db.viewmodels.ObcutjeViewModel;
import com.rvir.theboaringapp.util.InteresAPIsHandler;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ObcutjeViewModel obcutjeViewModel;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button gumb = findViewById(R.id.zacni_button);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        obcutjeViewModel = ViewModelProviders.of(this).get(ObcutjeViewModel.class);

        new PridobiCitat(this).execute();

        gumb.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), VprasalnikActivity.class);
            startActivity(intent);
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_layout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.nastavitve:
                System.out.println("nastavitve klik");
                Intent i = new Intent(this, InteresiActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private static class PridobiCitat extends AsyncTask<Void, Void, String> {

        private WeakReference<MainActivity> activityReference;
        private ProgressDialog pDialog;

        PridobiCitat(MainActivity context) {
            activityReference = new WeakReference<>(context);
            pDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            pDialog.setMessage("Prosimo počakajte...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(Void... arg0) {
            return new InteresAPIsHandler().getQuoteFromApi();
        }

        @Override
        protected void onPostExecute(String citat) {

            MainActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

        }
    }
}
