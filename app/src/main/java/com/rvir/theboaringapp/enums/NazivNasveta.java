package com.rvir.theboaringapp.enums;

import android.content.Intent;

public enum NazivNasveta {
    NIC("sights"),   // 1
    FILM("film"),   // 2
    SERIJA("serija"),   // 3
    KUHANJE("kuhanje"), // 4
    GLASBA("poslušanje glasbe"),    // 5 http://ws.audioscrobbler.com/2.0/?method=chart.getTopTracks&api_key=12eed98f620b02fbc2de23189014a86d&format=json&limit=10
    // tu spodaj so Google Maps searchi
    NOGOMET("football_field"),  // 6
    KOSARKA("basketball_field"),    // 7
    BOWLING("bowling_alley"),   // 8
    TEK("stadium"), // 9
    SPREHOD("park"),    // 10
    MUZEJ("museum"),    // 11
    TOPLICE("spa"), // 12
    FITNES("gym"),  // 13
    PAINTBALL("paintball"), // 14
    TENIS("tennis"),   // 15
    PIJACKA("cafe OR bar OR pub");  // 16


    private String vrsta;

    private NazivNasveta(String vrsta) {
        this.vrsta = vrsta;
    }

    public String getVrsta() {
        return this.vrsta;
    }

}
